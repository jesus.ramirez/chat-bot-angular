export interface Chat {

    text: string;
    sessionId?: string;
    type: string;
    email?: string;
    name?: string;
    node: string;

}
