export interface Client {

    Id: string;
    Name: string;
    Email: string;
    Phone: number;
    Company: string;
    City: string;
    Country: string;

}
