import { Injectable } from '@angular/core';
import { Api } from 'src/providers/Api';

@Injectable({
    providedIn: 'root'
})
export class FormularioService {

    url: string;
    http: any;

    constructor(private api: Api) { }

    Signup(Data) {
        return new Promise((resolve, reject) => {

            // tslint:disable-next-line:no-shadowed-variable
            const obs = this.api.post('signup', Data).subscribe((Data) => {
                resolve(Data);
                return obs;
            });
        });
    }
}
