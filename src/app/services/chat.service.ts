import { Injectable } from '@angular/core';
import { Api } from 'src/providers/Api';
// import { Socket } from 'ngx-socket-io';

@Injectable({
    providedIn: 'root'
})
export class ChatService {

    constructor(private api: Api) { }

    Messenger(Data) {
        return new Promise((resolve, reject) => {

            // tslint:disable-next-line:no-shadowed-variable
            const obs = this.api.post('conversation', Data).subscribe((Data) => {
                resolve( Data );
                return obs;
            },
                (error) => {
                    reject(error);
                });

        });
    }
}
