import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { Api } from 'src/providers/Api';

import { ChatComponent } from './chat/chat.component';
import { BubbleComponent } from './chat/bubble/bubble.component';
import { ButtonComponent } from './chat/bubble/button/button.component';
import { ImageComponent } from './chat/bubble/image/image.component';
import { CarrouselComponent } from './chat/bubble/carrousel/carrousel.component';
import { CardImgTopComponent } from './chat/bubble/card/link/card-img-top/card-img-top.component';
import { VideoComponent } from './chat/bubble/video/video.component';
import { CardSimpleLinkComponent } from './chat/bubble/card/link/card-simple-link/card-simple-link.component';
import { CardImgLeftLinkComponent } from './chat/bubble/card/link/card-img-left-link/card-img-left-link.component';
import { CardImgRightLinkComponent } from './chat/bubble/card/link/card-img-right-link/card-img-right-link.component';
import { CardSimpleButtonComponent } from './chat/bubble/card/button/card-simple-button/card-simple-button.component';
import { CardImgTopButtonComponent } from './chat/bubble/card/button/card-img-top-button/card-img-top-button.component';
import { CardImgRightButtonComponent } from './chat/bubble/card/button/card-img-right-button/card-img-right-button.component';
import { CardImgLeftButtonComponent } from './chat/bubble/card/button/card-img-left-button/card-img-left-button.component';
import { CardVideoLinkComponent } from './chat/bubble/card/link/card-video-link/card-video-link.component';
import { CardVideoButtonComponent } from './chat/bubble/card/button/card-video-button/card-video-button.component';
// import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

// const config: SocketIoConfig = { url: 'http://localhost:4000', options: {} };
@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    BubbleComponent,
    ButtonComponent,
    ImageComponent,
    CarrouselComponent,
    CardImgTopComponent,
    VideoComponent,
    CardSimpleLinkComponent,
    CardImgLeftLinkComponent,
    CardImgRightLinkComponent,
    CardSimpleButtonComponent,
    CardImgTopButtonComponent,
    CardImgRightButtonComponent,
    CardImgLeftButtonComponent,
    CardVideoLinkComponent,
    CardVideoButtonComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    // SocketIoModule.forRoot(config)
  ],
  providers: [
    Api
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
