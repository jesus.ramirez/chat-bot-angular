import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ChatComponent } from './chat.component';

describe('Test Bubble', () => {
    const component = new ChatComponent;


    it('suma', () => {
        const result = component.sumar(3, 3);

        expect(result).toEqual(6);
    });

    it('formulario', () => {

        const result = component.doSignup();

        expect(result);

    });

    it('button', () => {

        const func = component.clicked({ textButton: 'string', text: 'any', type: 'string', email: 'string', });

        expect(func);

    });

});
