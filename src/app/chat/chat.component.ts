import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Chat } from 'src/models/chat.model';
import { ChatService } from '../services/chat.service';
import { FormularioService } from '../services/formulario.service';
declare var window: any;

declare var responsiveVoice;

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {


  mensaje: Array<Chat>;
  account: any = {};
  chat = false;
  form = true;

  speak = false;

  uId = '';
  mail: string;
  name: string;
  node = '';

  text: any;
  constructor(public chatService: ChatService, private signupService: FormularioService) {
  }

  ngOnInit() {
    this.mensaje = [];

    // const scrolling = document.getElementById('chat-box-body');

    // scrolling.scrollTop = scrolling.scrollHeight;

  }




  // speack

  speackToText() {
    window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;

    const recognition = new SpeechRecognition();
    recognition.interimResults = true;


    recognition.addEventListener('result', e => {

      const trascript = Array.from(e.results)
        .map(result => result[0])
        .map(result => result.transcript)
        .join('');

      this.text = trascript;


      console.log(trascript);
    });


    recognition.addEventListener('end', recognition.stop);

    recognition.start();

    this.text = '';

  }


  // formulario

  doSignup(terminos: boolean) {


    console.log(this.account);

    if (terminos === true && this.account.Name !== '' && this.account.Phone !== '' && this.account.Email !== '') {

      localStorage.setItem('name', this.account.Name);
      localStorage.setItem('phone', this.account.Phone);

      this.signupService.Signup(this.account).then((res: any) => console.log(res.message));
      this.chat = true;
      this.form = false;

      const email = this.account.Email;
      this.mail = email;

      const envio = {
        text: 'empezar',
        node: this.node,
        email: this.mail,
        sessionId: '',
        type: 'user',
      };

      console.log(envio);

      this.chatService.Messenger(envio).then((res: Chat) => {

        const resp = res;

        this.uId = resp.sessionId;
        this.node = resp.node;

        console.log(res);

        if (this.speak === true) {

          responsiveVoice.speak(resp.text[0], 'Spanish Female', { pitch: 1 });

        }

        this.name = this.account.Name;

        const comp = resp.text[0];

        const name = comp.indexOf('_nombre');

        if (name !== -1) {

          const text = resp.text[0];


          const nombre = text.replace('_nombre', this.name);

          resp.text = nombre;

          this.mensaje.push(resp);

        } else {

          this.mensaje.push(resp);

        }
      });
    } else {

      alert('Por favor ingrese todos los campos');

    }


  }



  // chat


  onTaskAdd(event) {
    const textInput = document.getElementById('chat-input');
    (textInput as HTMLInputElement).value = '';
  }


  clicked(mensajes) {

    const envio = {
      text: mensajes,
      node: this.node,
      sessionId: this.uId,
      type: 'user',
      email: this.mail
    };

    this.mensaje.push(envio);

    this.chatService.Messenger(envio).then((res: Chat) => {

      this.node = res.node;

      const name = res.text[0].indexOf('_nombre');

      console.log(res.text);



      if (name !== -1) {
        for (let index = 0; index < res.text.length; index++) {
          const messenge = res.text[index];


          const respuesta = {
            sessionId: res.sessionId,
            text: messenge,
            type: res.type,
            node: res.node
          };


          const text = messenge;


          const nombre = text.replace('_nombre', this.name);

          respuesta.text = nombre;

          this.mensaje.push(respuesta);
          this.scrollTop();
        }

      } else {

        for (let index = 0; index < res.text.length; index++) {
          const messenge = res.text[index];


          const respuesta = {
            sessionId: res.sessionId,
            text: messenge,
            type: res.type,
            node: res.node
          };

          this.mensaje.push(respuesta);


          this.scrollTop();

        }

      }
    });

  }


  generarBubble() {
    const envio = {
      text: (document.getElementById('chat-input') as HTMLInputElement).value,
      sessionId: this.uId,
      node: this.node,
      type: 'user',
      email: this.mail

    };

    if (envio.text === '') {

      alert('por favor poner un mensaje');

    } else {

      console.log(envio);
      this.mensaje.push(envio);
      this.scrollTop();
      this.chatService.Messenger(envio).then((res: Chat) => {
        const resp = res;
        const comp = resp.text[0];
        this.node = resp.node;

        const name = comp.indexOf('_nombre');
        const split = comp.indexOf('","');

        if (split !== -1) {
          console.log('dio');
        }




        if (name !== -1) {

          if (name !== -1) {
            for (let index = 0; index < res.text.length; index++) {
              const messenge = res.text[index];


              const respuesta = {
                sessionId: res.sessionId,
                text: messenge,
                type: res.type,
                node: res.node
              };



              const nombre = respuesta.text.replace('_nombre', this.name);

              resp.text = nombre;


              this.mensaje.push(respuesta);
              this.scrollTop();

            }
          }

        } else {

          for (let index = 0; index < res.text.length; index++) {
            const messenge = res.text[index];


            const respuesta = {
              sessionId: res.sessionId,
              text: messenge,
              type: res.type,
              node: res.node
            };


            this.mensaje.push(respuesta);
            this.scrollTop();

          }

        }

        if (this.speak === true) {

          responsiveVoice.speak(resp.text[0], 'Spanish Female', { pitch: 1 });

        }

      });
      const textInput = document.getElementById('chat-input');
      (textInput as HTMLInputElement).value = '';
    }
  }
  // AED5F0 BOT
  // 006EC9 USER
  // #1C9FDA BOTONES

  scrollTop() {
    setTimeout(function () {
      let chatBox, message;
      chatBox = document.querySelector('#chat-box-body');
      message = document.querySelector('#chat-input');
      // aca lo interesante
      chatBox.scrollTop = chatBox.scrollHeight;
      // expected output: 'resolved'
    }, 500);

  }
}

