import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-carrousel',
  templateUrl: './carrousel.component.html',
  styleUrls: ['./carrousel.component.scss'],
  providers: [ NgbCarouselConfig ]
})
export class CarrouselComponent implements OnInit {

  constructor(private sixe: DomSanitizer, config: NgbCarouselConfig) {
    config.interval = 10000;
    config.wrap = false;
    config.keyboard = false;
    config.pauseOnHover = false;
    config.showNavigationArrows = false;
   }

  @Input() mensajes: { url: string, title: string, text: string, textButton: string, link: string;};

  ngOnInit() {}

  sanitize(url: string) {
    return this.sixe.bypassSecurityTrustUrl(url);
  }

}
