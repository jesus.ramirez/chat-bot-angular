import { Component, OnInit, Input } from '@angular/core';
import { Chat } from 'src/models/chat.model';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  constructor() { }

  @Input() mensajes: { text: string, type: string, textbutton: string, mensaje: string };



  ngOnInit() {
  }

}
