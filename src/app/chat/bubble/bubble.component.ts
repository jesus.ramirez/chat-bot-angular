import { Component, OnInit, Input } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { ChatService } from '../../services/chat.service';
import { ChatComponent } from '../chat.component';

@Component({
  selector: 'app-bubble',
  templateUrl: './bubble.component.html',
  styleUrls: ['./bubble.component.scss'],
  providers: [NgbCarouselConfig]
})
export class BubbleComponent implements OnInit {

  constructor(public chatService: ChatService, private click: ChatComponent, config: NgbCarouselConfig) {

    config.interval = 10000;
    config.wrap = false;
    config.keyboard = false;
    config.pauseOnHover = false;
    config.showNavigationIndicators = false;
   }

  botones: boolean;
  boton: boolean;
  img: boolean;
  carrousel: boolean;
  linstCard: boolean;
  video: boolean;

  // cards link

  cardSimpleLink: boolean;
  cardTopLink: boolean;
  cardLeftLink: boolean;
  cardRightLink: boolean;
  cardVideoLink: boolean;

  // cards Button

  cardSimpleButton: boolean;
  cardTopButton: boolean;
  cardLeftButton: boolean;
  cardRightButton: boolean;
  cardVideoButton: boolean;


  // cards link

  cardSimpleLinkList: boolean;
  cardTopLinkList: boolean;
  cardLeftLinkList: boolean;
  cardRightLinkList: boolean;
  cardVideoLinkList: boolean;

  // cards Button

  cardSimpleButtonList: boolean;
  cardTopButtonList: boolean;
  cardLeftButtonList: boolean;
  cardRightButtonList: boolean;
  cardVideoButtonList: boolean;


  // cards

  @Input() cardDateSimpleLink: any; cardDateLeftleLink: any; cardDateRightleLink: any; cardDateTopLink: any; cardDateVideoLink: any;

  @Input() cardDateSimpleButton: any; cardDateLeftleButton: any; cardDateRightleButton: any; cardDateTopButton: any; cardDateVideoButton: any;

  // cards list

  @Input() cardDateSimpleLinkList: any; cardDateLeftLinkList: any; cardDateRightLinkList: any; cardDateTopLinkList: any; cardDateVideoLinkList: any;

  @Input() cardDateSimpleButtonList: any; cardDateLeftButtonList: any; cardDateRightButtonList: any; cardDateTopButtonList: any; cardDateVideoButtonList: any;

  @Input() mensajes: { text: string, type: string, textbutton: string, botones: any, name: string }; array: any; cardList: any; videoDate: any;


  ngOnInit() {



    // tratamiento de datos
    const comp = JSON.stringify(this.mensajes.text);


    // boton
    const btn = comp.indexOf('_btn');

    // botones
    const btns = comp.indexOf('_botones');

    // imagen
    const img = comp.indexOf('_img');

    // carrousel

    const carrousel = comp.indexOf('_carrusel');


    // list card

    const listCard = comp.indexOf('_lista');

    // video

    const video = comp.indexOf('_video');





    // card-simple-link

    const cardSimpleLink = comp.indexOf('_card-link-simple');

    // card-img-top-link

    const cardTopLink = comp.indexOf('_card-img-link-top');

    // card-img-rleft-link

    const cardLeftLink = comp.indexOf('_card-img-link-left');

    // card-img-rigth-link

    const cardRightLink = comp.indexOf('_card-img-link-right');

    // card video link

    const cardVideoLink = comp.indexOf('_card-link-video');





    // card-simple-Button

    const cardSimpleButton = comp.indexOf('_card-simple-button');

    // card-img-top-button

    const cardTopButton = comp.indexOf('_card-img-top-button');

    // card-img-rleft-button

    const cardLeftButton = comp.indexOf('_card-img-left-button');

    // card-img-rigth-button

    const cardRightButton = comp.indexOf('_card-img-right-button');

    // card video button

    const cardVideoButton = comp.indexOf('_card-video-button');



// ================ List =============================================== //


    // card-simple-link

    const cardSimpleLinkList = comp.indexOf('_card-list-simple-link');

    // card-img-top-link

    const cardTopLinkList = comp.indexOf('_card-list-img-top-link');

    // card-img-rleft-link

    const cardLeftLinkList = comp.indexOf('_card-list-img-left-link');

    // card-img-rigth-link

    const cardRightLinkList = comp.indexOf('_card-list-img-right-link');




    // card-simple-Button

    const cardSimpleButtonList = comp.indexOf('_card-list-simple-button');

    // card-img-top-button

    const cardTopButtonList = comp.indexOf('_card-list-img-top-button');

    // card-img-rleft-button

    const cardLeftButtonList = comp.indexOf('_card-list-img-left-button');

    // card-img-rigth-button

    const cardRightButtonList = comp.indexOf('_card-list-img-top-button');



    // boton
    if (btn !== -1) {

      this.boton = true;

      const datos = comp.split('_btn');
      const mensaje = datos[0].replace('[', '');
      const mensajeBoton = datos[1].replace('"]', '');

      // mensaje boton
      this.mensajes.textbutton = mensajeBoton;

      // mensaje
      this.mensajes.text = mensaje;

    }




    // botones con tratamiento de datos
    if (btns !== -1) {
      this.botones = true;

      const datos = comp.split('_botones');
      const mensaje = datos[0].replace('"', '').replace('[', '');
      const botones = datos[1].replace('"]', '').replace('"', '').split(',');

      this.array = botones;
      // mensaje
      this.mensajes.text = mensaje;

    }



    // imagen
    if (img !== -1) {
      this.img = true;
      const datos = comp.split('_img');
      const mensaje = datos[0].replace('["', '');
      const mensajeImg = datos[1].replace(' ', '');

      // mensaje img
      this.mensajes.textbutton = mensajeImg;

      // mensaje
      this.mensajes.text = mensaje;
    }



    // carrusel

    if (carrousel !== -1) {
      this.carrousel = true;
      const datos = comp.split('_carrusel ');
      const mensaje = datos[0].replace('["', '');
      const car = datos[1].replace(']"', '').split('**');

      const listas = [];

      for (let index = 0; index < car.length; index++) {
        const element = car[index];
        const item = element.split(',');

        let json = {};

          json = {
            url: item[0],
            title: item[1],
            text: item[2],
            textButton: item[3],
            link: item[4],
          };
          listas.push(json);

      }


      this.cardList = listas;
      // mensaje
      this.mensajes.text = mensaje;

    }


    // lista de tarjetas

    if (listCard !== -1) {
      this.linstCard = true;

      const datos = comp.split('_lista');
      const mensaje = datos[0].replace('["', '');

      const lista = datos[1].replace('"]', '').replace(' ', '').split('**');

      const listas = [];


      for (let index = 0; index < lista.length; index++) {
        const element = lista[index];
        const item = element.split(',');

        const json = {
          url: item[0],
          title: item[1],
          text: item[2],
          textButton: item[3],
          link: '',
        };
        listas.push(json);
      }

      this.cardList = listas;


      this.mensajes.text = mensaje;

    }


    // video

    if (video !== -1) {

      this.video = true;

      const datos = comp.split('_video');
      const mensaje = datos[0].replace('["', '');
      const videos = datos[1].replace('"]', '').replace(' ', '');

      console.log('video', videos);

      this.mensajes.text = mensaje;
      this.videoDate = {
        link: videos
      };

    }




    // card-simple-link


    if (cardSimpleLink !== -1) {

      this.cardSimpleLink = true;

      const data = comp.split('_card-link-simple');
      const mensaje = data[0].replace('["', '').replace('"', '');

      const cardDate = data[1].split(',');

      this.cardDateSimpleLink = {
        title: cardDate[0],
        text: cardDate[1],
        textButton: cardDate[2],
        link: cardDate[3].replace('"]', '')
      };

      this.mensajes.text = mensaje;


    }


    // card-top-link


    if (cardTopLink !== -1) {

      this.cardTopLink = true;

      const datos = comp.split('_card-img-link-top');
      const mensaje = datos[0].replace('["', '');

      const datosCard = datos[1].split(',');



      this.mensajes.text = mensaje;


      this.cardDateTopLink = {
        url: datosCard[0],
        title: datosCard[1],
        text: datosCard[2],
        textButton: datosCard[3],
        link: datosCard[4].replace('"]', '')
      };

      console.log(this.cardDateTopLink);
    }



    // card-left-link


    if (cardLeftLink !== -1) {

      this.cardLeftLink = true;

      const data = comp.split('_card-img-link-left');
      const mensaje = data[0].replace('["', '');
      const cardDate = data[1].split(',');


      this.cardDateLeftleLink = {
        url: cardDate[0],
        title: cardDate[1],
        text: cardDate[2],
        textButton: cardDate[3],
        link: cardDate[4].replace('"]', '')
      };


      this.mensajes.text = mensaje;

    }


    // card-Right-link

    if (cardRightLink !== -1) {

      this.cardRightLink = true;

      const data = comp.split('_card-img-link-right');
      const mensaje = data[0].replace('["', '');
      const cardDate = data[1].split(',');

      this.cardDateRightleLink = {
        url: cardDate[0],
        title: cardDate[1],
        text: cardDate[2],
        textButton: cardDate[3],
        link: cardDate[4].replace('"]', '')
      };


      this.mensajes.text = mensaje;

    }




    // card-simple-Button


    if (cardSimpleButton !== -1) {

      this.cardSimpleButton = true;

      const data = comp.split('_card-simple-button');
      const mensaje = data[0].replace('["', '');

      const cardDate = data[1].split(',');

      this.cardDateSimpleButton = {
        title: cardDate[0],
        text: cardDate[1],
        textButton: cardDate[2].replace('"]', '')
        // link: cardDate[3].replace('"]', '')
      };

      this.mensajes.text = mensaje;


    }


    // card-top-button


    if (cardTopButton !== -1) {

      this.cardTopButton = true;

      const datos = comp.split('_card-img-top-button');
      const mensaje = datos[0].replace('["', '');

      const datosCard = datos[1].split(',');



      this.mensajes.text = mensaje;


      this.cardDateTopButton = {
        url: datosCard[0],
        title: datosCard[1],
        text: datosCard[2],
        textButton: datosCard[3].replace('"]', '')
        // link: datosCard[4].replace('"]', '')
      };
    }



    // card-left-button


    if (cardLeftButton !== -1) {

      this.cardLeftButton = true;

      const data = comp.split('_card-img-left-button');
      const mensaje = data[0].replace('["', '');
      const cardDate = data[1].split(',');


      this.cardDateLeftleButton = {
        url: cardDate[0],
        title: cardDate[1],
        text: cardDate[2],
        textButton: cardDate[3].replace('"]', '')
        // link: cardDate[4].replace('"]', '')
      };


      this.mensajes.text = mensaje;

    }


    // card-Right-button

    if (cardRightButton !== -1) {

      this.cardRightButton = true;

      const data = comp.split('_card-img-right-button');
      const mensaje = data[0].replace('"[', '');
      const cardDate = data[1].split(',');

      this.cardDateRightleButton = {
        url: cardDate[0],
        title: cardDate[1],
        text: cardDate[2],
        textButton: cardDate[3].replace('"]', '')
        // link: cardDate[4].replace('"]', '')
      };


      this.mensajes.text = mensaje;

    }

    // card video link

    if (cardVideoLink !== -1) {

      this.cardVideoLink = true;

      const data = comp.split('_card-video-link');
      const mensaje = data[0].replace('["', '');
      const cardDate = data[1].split(',');

      this.cardDateVideoLink = {
        url: cardDate[0],
        title: cardDate[1],
        text: cardDate[2],
        textButton: cardDate[3],
        link: cardDate[4].replace('"]', '')
      };

      this.mensajes.text = mensaje;


    }


    // card video button

    if (cardVideoButton !== -1) {

      this.cardVideoButton = true;

      const data = comp.split('_card-video-button');
      const mensaje = data[0].replace('["', '');
      const cardDate = data[1].split(',');

      this.cardDateVideoButton = {
        url: cardDate[0],
        title: cardDate[1],
        text: cardDate[2],
        textButton: cardDate[3].replace('"]', '')
      };

      this.mensajes.text = mensaje;


    }





    // ===============list cards ============================= //






    // card-simple-link


    if (cardSimpleLinkList !== -1) {
      this.cardSimpleLinkList = true;

      const datos = comp.split('_card-list-simple-link');
      const mensaje = datos[0].replace('["', '');

      const lista = datos[1].replace('"]', '').replace(' ', '').split('**');

      const listas = [];


      for (let index = 0; index < lista.length; index++) {
        const element = lista[index];
        const item = element.split(',');

        const json = {
          title: item[0],
          text: item[1],
          textButton: item[2],
          link: item[3]
        };
        listas.push(json);
      }

      this.cardDateSimpleLinkList = listas;


      this.mensajes.text = mensaje;

    }


    // card-top-link


    if (cardTopLinkList !== -1) {

      this.cardTopLinkList = true;

      const datos = comp.split('_card-list-img-top-link');
      const mensaje = datos[0].replace('["', '');

      const lista = datos[1].replace('"]', '').replace(' ', '').split('**');

      const listas = [];


      for (let index = 0; index < lista.length; index++) {
        const element = lista[index];
        const item = element.split(',');

        const json = {
          url: item[0],
          title: item[1],
          text: item[2],
          textButton: item[3],
          link: item[4]
        };
        listas.push(json);
      }

      this.cardDateTopLinkList = listas;


      this.mensajes.text = mensaje;
    }



    // card-left-link


    if (cardLeftLinkList !== -1) {

      this.cardLeftLinkList = true;

      const datos = comp.split('_card-list-img-left-link');
      const mensaje = datos[0].replace('["', '');

      const lista = datos[1].replace('"]', '').replace(' ', '').split('**');

      const listas = [];


      for (let index = 0; index < lista.length; index++) {
        const element = lista[index];
        const item = element.split(',');

        const json = {
          url: item[0],
          title: item[1],
          text: item[2],
          textButton: item[3],
          link: item[4]
        };
        listas.push(json);
      }

      this.cardDateLeftLinkList = listas;


      this.mensajes.text = mensaje;

    }


    // card-Right-link

    if (cardRightLinkList !== -1) {

      this.cardRightLinkList = true;

      const datos = comp.split('_card-list-img-right-link');
      const mensaje = datos[0].replace('["', '');

      const lista = datos[1].replace('"]', '').replace(' ', '').split('**');

      const listas = [];


      for (let index = 0; index < lista.length; index++) {
        const element = lista[index];
        const item = element.split(',');

        const json = {
          url: item[0],
          title: item[1],
          text: item[2],
          textButton: item[3],
          link: item[4]
        };
        listas.push(json);
      }

      this.cardDateRightLinkList = listas;


      this.mensajes.text = mensaje;


    }




    // card-simple-Button


    if (cardSimpleButtonList !== -1) {


      this.cardSimpleButtonList = true;

      const datos = comp.split('_card-list-simple-button');
      console.log(datos);
      const mensaje = datos[0].replace('["', '');

      const lista = datos[1].replace('"]', '').replace(' ', '').split('**');

      const listas = [];


      for (let index = 0; index < lista.length; index++) {
        const element = lista[index];
        const item = element.split(',');

        const json = {
          title: item[0],
          text: item[1],
          textButton: item[2]
        };
        listas.push(json);
      }

      console.log(listas);

      this.cardDateSimpleButtonList = listas;


      this.mensajes.text = mensaje;
    }


    // card-top-button


    if (cardTopButtonList !== -1) {

      this.cardTopButtonList = true;

      const datos = comp.split('_card-list-img-top-button');
      const mensaje = datos[0].replace('["', '');

      const lista = datos[1].replace('"]', '').replace(' ', '').split('**');

      const listas = [];


      for (let index = 0; index < lista.length; index++) {
        const element = lista[index];
        const item = element.split(',');

        const json = {
          url: item[0],
          title: item[1],
          text: item[2],
          textButton: item[3]
        };
        listas.push(json);
      }

      this.cardDateTopButtonList = listas;


      this.mensajes.text = mensaje;
    }



    // card-left-button


    if (cardLeftButtonList !== -1) {

      this.cardLeftButtonList = true;

      const datos = comp.split('_card-list-img-left-button');
      const mensaje = datos[0].replace('["', '');

      const lista = datos[1].replace('"]', '').replace(' ', '').split('**');

      const listas = [];


      for (let index = 0; index < lista.length; index++) {
        const element = lista[index];
        const item = element.split(',');

        const json = {
          url: item[0],
          title: item[1],
          text: item[2],
          textButton: item[3]
        };
        listas.push(json);
      }

      this.cardDateLeftButtonList = listas;


      this.mensajes.text = mensaje;
    }


    // card-Right-button

    if (cardRightButtonList !== -1) {

      this.cardRightButtonList = true;

      const datos = comp.split('_card-list-img-top-button');
      const mensaje = datos[0].replace('["', '');

      const lista = datos[1].replace('"]', '').replace(' ', '').split('**');

      const listas = [];


      for (let index = 0; index < lista.length; index++) {
        const element = lista[index];
        const item = element.split(',');

        const json = {
          url: item[0],
          title: item[1],
          text: item[2],
          textButton: item[3]
        };
        listas.push(json);
      }

      this.cardDateRightButtonList = listas;


      this.mensajes.text = mensaje;

    }


  }

  // clickbotones

  clicked(mensaje) {
    this.click.clicked(mensaje);
  }


}
