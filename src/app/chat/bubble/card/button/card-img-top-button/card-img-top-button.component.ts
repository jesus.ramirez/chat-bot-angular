import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BubbleComponent } from '../../../bubble.component';

@Component({
  selector: 'app-card-img-top-button',
  templateUrl: './card-img-top-button.component.html',
  styleUrls: ['./card-img-top-button.component.scss']
})
export class CardImgTopButtonComponent implements OnInit {

  constructor(private sixe: DomSanitizer, public click: BubbleComponent) { }

  @Input() mensajes: { url: string, title: string, text: string, textButton: string, link: string; };

  ngOnInit() {
  }

  sanitize(url: string) {
    return this.sixe.bypassSecurityTrustUrl(url);
  }

  clicked(click) {
    this.click.clicked(click);
  }

}
