import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BubbleComponent } from '../../../bubble.component';

@Component({
  selector: 'app-card-simple-button',
  templateUrl: './card-simple-button.component.html',
  styleUrls: ['./card-simple-button.component.scss']
})
export class CardSimpleButtonComponent implements OnInit {

  constructor(private sixe: DomSanitizer, public click: BubbleComponent) { }

  @Input() mensajes: { url: string, title: string, text: string, textButton: string, link: string; };

  ngOnInit() {
  }

  sanitize(url: string) {
    return this.sixe.bypassSecurityTrustUrl(url);
  }


  clicked(click) {
    this.click.clicked(click);
  }
}
