import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BubbleComponent } from '../../../bubble.component';

@Component({
  selector: 'app-card-video-button',
  templateUrl: './card-video-button.component.html',
  styleUrls: ['./card-video-button.component.scss']
})
export class CardVideoButtonComponent implements OnInit {

  constructor(private sixe: DomSanitizer, public click: BubbleComponent) { }

  @Input() mensajes: { url: string, title: string, text: string, textButton: string, link: string; };

  ngOnInit() {
  }

  sanitize(url: string) {
    return this.sixe.bypassSecurityTrustResourceUrl(url);
  }

  clicked(click) {
    this.click.clicked(click);
  }

}
