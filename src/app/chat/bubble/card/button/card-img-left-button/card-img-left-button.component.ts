import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BubbleComponent } from '../../../bubble.component';

@Component({
  selector: 'app-card-img-left-button',
  templateUrl: './card-img-left-button.component.html',
  styleUrls: ['./card-img-left-button.component.scss']
})
export class CardImgLeftButtonComponent implements OnInit {


  constructor(private sixe: DomSanitizer, public click: BubbleComponent) { }

  @Input() mensajes: { url: string, title: string, text: string, textButton: string, link: string; };

  ngOnInit() {
  }

  sanitize(url: string) {
    return this.sixe.bypassSecurityTrustUrl(url);
  }

  clicked(click) {
    this.click.clicked(click);
  }

}
