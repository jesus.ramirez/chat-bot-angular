import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-card-simple-link',
  templateUrl: './card-simple-link.component.html',
  styleUrls: ['./card-simple-link.component.scss']
})
export class CardSimpleLinkComponent implements OnInit {


  constructor(private sixe: DomSanitizer) { }

  @Input() mensajes: { url: string, title: string, text: string, textButton: string, link: string; };

  ngOnInit() {
  }

  sanitize(url: string) {
    return this.sixe.bypassSecurityTrustUrl(url);
  }

}
