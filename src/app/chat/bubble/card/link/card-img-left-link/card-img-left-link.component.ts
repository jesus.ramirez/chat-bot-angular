import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-card-img-left-link',
  templateUrl: './card-img-left-link.component.html',
  styleUrls: ['./card-img-left-link.component.scss']
})
export class CardImgLeftLinkComponent implements OnInit {

  constructor(private sixe: DomSanitizer) { }

  @Input() mensajes: { url: string, title: string, text: string, textButton: string, link: string; };

  ngOnInit() {
  }

  sanitize(url: string) {
    return this.sixe.bypassSecurityTrustUrl(url);
  }


}
