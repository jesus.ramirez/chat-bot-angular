import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-card-img-top-link',
  templateUrl: './card-img-top.component.html',
  styleUrls: ['./card-img-top.component.scss']
})
export class CardImgTopComponent implements OnInit {

  constructor(private sixe: DomSanitizer) { }

  @Input() mensajes: { url: string, title: string, text: string, textButton: string, link: string; };

  ngOnInit() {
  }

  sanitize(url: string) {
    return this.sixe.bypassSecurityTrustUrl(url);
  }


}
