import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-card-video-link',
  templateUrl: './card-video-link.component.html',
  styleUrls: ['./card-video-link.component.scss']
})
export class CardVideoLinkComponent implements OnInit {

  constructor(private sixe: DomSanitizer) { }

  @Input() mensajes: { url: string, title: string, text: string, textButton: string, link: string; };

  ngOnInit() {
  }

  sanitize(url: string) {
    return this.sixe.bypassSecurityTrustResourceUrl(url);
  }

}
