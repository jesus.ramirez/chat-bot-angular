import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {

  constructor(private sixe: DomSanitizer) { }

  @Input() mensajes: { text: string, type: string, textbutton: string, mensaje: string };

  ngOnInit() {
  }

  sanitize(url: string) {
    return this.sixe.bypassSecurityTrustUrl(url);
  }


}
