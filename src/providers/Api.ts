import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { AppSettings } from 'src/app/constants';
import { Observable } from 'rxjs';


@Injectable()
export class Api {
    url: String = AppSettings.Api;
    public headers;

    constructor(public http: HttpClient) { }

    post(endpoint: string, body: any) {
        this.headers = new Headers();
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.post(this.url + '/' + endpoint, body);
    }

    get(endpoint: string, body: any) {
        // const headers:HttpHeaders  = new HttpHeaders();
        return this.http.get(this.url + '/' + endpoint, body);
    }
}
