#Font-end de chatbot en angular 7.3.8

Este proyecto esta seccionado en varias partes:

- **server.js:** es donde se inicia el servidor de node para desplegar la aplicacion compilada.
- **src:** donde esta todo el proyecto
	- **app:** Toda la aplicacion.
	- **models:** Modelo de datos que resive la aplicacion.
	- **Providers:** Es donde estan hubicados los metodos de comunicacion http.



***

#src

##providers

Dentro de este se encuentra:

- **Api.ts:** Este contiene los metodos de comunicacion http:
	- ` post(endpoint: string, body: any)`: Este metodo(post) resive el endpoint a donde se va a hacer la peticion y la data que va a mandar.
	- `get(endpoint: string, body: any)`: Este metodo(get) resive como parametros el endpoint donde se va a solicitar la informacion y el retorna la informacion entrante de este.


##models

Dentro de este se encuentran los siguientes archivos:

- **chat.model.ts:** Esta este es un modelo de datos para el intercambio de infotmaciond de las burbujas.

- **client.model.ts:** Este es un modelo de datos para el ingresode informacion para el formulario de inicio.


##app

- **constants.ts:** En esta clase se maneja la direccion url del API global sin (/) a el final.

---
###services

- **chat.service.ts:** En este se maneja los metodos que hacen la mnsajeria de el chat posible.
	- **`Messeger(data)`:** Este metodo resive como parametro toda la data que se va a enviar a el backend y el retorna la espuesta de el backend.

- **formulario.service.ts:** Este servicio maneja el metodo de envio de el formulario a el back end.
	- **`Signup(data)`:** Este metodo resive como parametro toda la data que se va a enviar a el backend para el formulario.


---

###chat

Este componente consiste en manejar el sistema de chat a gran escala donde el maneja la entrada y salida de mensajes de los servicios el reconocimiento de voz y tambioen la voz de el asistente virtual en caso que se necesita y la activacion o desactivacion de esta misma.

- En el controlador maneja varias propiedades:
	- si deseamos poner el asistente de voz podemos poner la propiedad (speak) en varadadero y el se activara.
	- la propiedad account va a contener todo el formulario para poderse enviar.
	- las propiedades chat y form manejan las vistas de el formulario cuando pasa de uno a otro.
	- la propiedad uid es para atrapar el id que nos da el back end para el reconocimiento.
	- la propiedad de (mail) se utiliza para manejarla internamente en el estado de la app al igual que (name).
	- la propiedad texto se maneja para modificar o atrapar el contenido de el input de entrada de el chat para poderlo modificar o hacer lo que queramos con esa informacion suministrada.

- **`ngOnInit()`:** este metodo va amanejar el estado de los mensajes que se van a ir generando en la conversacion.
- **`speackToText()`:** Este metodo maneja la trascripcion autometica con el reconocimiento de voz.
- **`doSignup(data)`:** Este metodo resive como parametro los datos de el formulario y el check de confirmacion para poder ser ejecutado y envia la infrmacion de el formulario a el back end.
- **`onTaskAdd()`:** este metodo limpia el input a el dar enter o click.
- **`clicked(mensajes)`:**este metodo maneja el evento click de todos los botones que manej ala palicacion para enviarlos como mensaje.
- **`generarBubble()`:** este se encarga de manejar el envio de mensajes por el input de mensajes y genera las burbujas de el chat.

---

###bubble

este componente es encargado de mostrar las burbujas de el chat y sus otras propiedades.

este componente tiene ciertas propiedades para mostar depende de la situacion en la que se necesite alguno de sus otros microcomponentes.

que son estos:

		// components simple

          botones: boolean;
          boton: boolean;
          img: boolean;
          carrousel: boolean;
          linstCard: boolean;
          video: boolean;

          // cards link

          cardSimpleLink: boolean;
          cardTopLink: boolean;
          cardLeftLink: boolean;
          cardRightLink: boolean;
          cardVideoLink: boolean;

          // cards Button

          cardSimpleButton: boolean;
          cardTopButton: boolean;
          cardLeftButton: boolean;
          cardRightButton: boolean;
          cardVideoButton: boolean;


          // cards List


          // cards link

          cardSimpleLinkList: boolean;
          cardTopLinkList: boolean;
          cardLeftLinkList: boolean;
          cardRightLinkList: boolean;
          cardVideoLinkList: boolean;

          // cards Button

          cardSimpleButtonList: boolean;
          cardTopButtonList: boolean;
          cardLeftButtonList: boolean;
          cardRightButtonList: boolean;
          cardVideoButtonList: boolean;


lo siguiente son los selectores y como se deben declarar para mostar cada uno de los ocmponenetes cuando se requieran.


ejemplo:

	const comp = JSON.stringify(this.mensajes.text);


    // boton
    const btn = comp.indexOf('_btn');

    // botones
    const btns = comp.indexOf('_botones');

    // imagen
    const img = comp.indexOf('_img');

    // carrousel

    const carrousel = comp.indexOf('_carrusel');


    // list card

    const listCard = comp.indexOf('_lista');

    // video

    const video = comp.indexOf('_video');


y se declara la condicion para generara y mostar los componentes respectivos a el selector.

ejemplo:

	if (btn !== -1) {

      this.boton = true;

      const datos = comp.split('_btn');
      const mensaje = datos[0].replace('["', '');
      const mensajeBoton = datos[1].replace('"]', '');

      // mensaje boton
      this.mensajes.textbutton = mensajeBoton;

      // mensaje
      this.mensajes.text = mensaje;

    }


de esta manera se compara el selector y si este sale diferente a -1 es por q pasa la condicion y por ende la propiedad boton se activa poniendola verdadera y haciendo la logica para despomponer el mensaje de watson y ponerlo como la informacion requerida para generar el boton.

---

Este es un componente que contiene otros micro componentes y que resiven un parametro con la siguinte extructura para su correctofuncionamiento.

- **`<app-button (click)="clicked(this.mensajes.textbutton)" [mensajes]='this.mensajes.textbutton'></app-button>`:** este por ser un boton debe tener un evento click con el metodo y el texto del boton como parametro.
Esta es la siguiente objeto que resive el componente en el campo mensajes:
		objeto: {
        	text: string,
            type: string,
            textbutton: string,
            mensaje: string
           }

- **`<app-image [mensajes]='this.img'></app-image>`:** Este maneja solo entrada de informacion con la url de la imagen a poner en el componente.

- **`<app-video [mensajes]='this.videoDate'></app-video>`:** Este compoente un parametro como el siguiente objeto:

		objeto = {
        	link: string
        }


- **`<app-card-img-top-link [mensajes]='cardDateTopLink'></app-card-img-top-link>`:** Este componente resive la siguinete estructura del objeto que resive como parametro:

		mensajes: {
        	url: string,
            title: string,
            text: string,
            textButton: string,
            link: string;
         };

- **`<app-card-simple-link [mensajes]='cardDateSimpleLink'></app-card-simple-link>`:** Este componente resive la siguinete estructura del objeto que resive como parametro:

		mensajes: {
        	url: string,
            title: string,
            text: string,
            textButton: string,
            link: string;
         };



- **`<app-card-img-left-link [mensajes]='cardDateLeftleLink'></app-card-img-left-link>`:** Este componente resive la siguinete estructura del objeto que resive como parametro:

		mensajes: {
        	url: string,
            title: string,
            text: string,
            textButton: string,
            link: string;
         };

- **`<app-card-img-right-link [mensajes]='cardDateRightleLink'></app-card-img-right-link>`:** Este componente resive la siguinete estructura del objeto que resive como parametro:

		mensajes: {
        	url: string,
            title: string,
            text: string,
            textButton: string,
            link: string;
         };


- **`<app-card-video-link [mensajes]='cardDateVideoLink'></app-card-video-link>`:** Este componente resive la siguinete estructura del objeto que resive como parametro:

		mensajes: {
        	url: string,
            title: string,
            text: string,
            textButton: string,
            link: string;
         };


- **`<app-card-img-top-button [mensajes]='cardDateTopButton'></app-card-img-top-button>`:** Este componente resive la siguinete estructura del objeto que resive como parametro:

		mensajes: {
        	url: string,
            title: string,
            text: string,
            textButton: string,
            link: string;
         };


- **`<app-card-simple-button [mensajes]='cardDateSimpleButton'></app-card-simple-button>`:** Este componente resive la siguinete estructura del objeto que resive como parametro:

		mensajes: {
        	url: string,
            title: string,
            text: string,
            textButton: string,
            link: string;
         };

- **`<app-card-img-left-button [mensajes]='cardDateLeftleButton'></app-card-img-left-button>`:** Este componente resive la siguinete estructura del objeto que resive como parametro:

		mensajes: {
        	url: string,
            title: string,
            text: string,
            textButton: string,
            link: string;
         };

- **`<app-card-img-right-button [mensajes]='cardDateRightleButton'></app-card-img-right-button>`:** Este componente resive la siguinete estructura del objeto que resive como parametro:

		mensajes: {
        	url: string,
            title: string,
            text: string,
            textButton: string,
            link: string;
         };


- **`<app-card-video-button [mensajes]='cardDateVideoButton'></app-card-video-button>`:** Este componente resive la siguinete estructura del objeto que resive como parametro:

		mensajes: {
        	url: string,
            title: string,
            text: string,
            textButton: string,
            link: string;
         };


##Estructura de contenido

        |   .editorconfig
        |   .gitignore
        |   angular.json
        |   package-lock.json
        |   package.json
        |   README.md
        |   server.js
        |   tsconfig.json
        |   tslint.json
        |
        \---src
            |   browserslist
            |   favicon.ico
            |   index.html
            |   karma.conf.js
            |   main.ts
            |   polyfills.ts
            |   styles.scss
            |   test.ts
            |   tsconfig.app.json
            |   tsconfig.spec.json
            |   tslint.json
            |
            +---app
            |   |   app.component.html
            |   |   app.component.scss
            |   |   app.component.spec.ts
            |   |   app.component.ts
            |   |   app.module.ts
            |   |   constants.ts
            |   |
            |   +---chat
            |   |   |   chat.component.html
            |   |   |   chat.component.scss
            |   |   |   chat.component.spec.ts
            |   |   |   chat.component.ts
            |   |   |
            |   |   \---bubble
            |   |       |   bubble.component.html
            |   |       |   bubble.component.scss
            |   |       |   bubble.component.spec.ts
            |   |       |   bubble.component.ts
            |   |       |
            |   |       +---button
            |   |       |       button.component.html
            |   |       |       button.component.scss
            |   |       |       button.component.spec.ts
            |   |       |       button.component.ts
            |   |       |
            |   |       +---card
            |   |       |   +---button
            |   |       |   |   +---card-img-left-button
            |   |       |   |   |       card-img-left-button.component.html
            |   |       |   |   |       card-img-left-button.component.scss
            |   |       |   |   |       card-img-left-button.component.spec.ts
            |   |       |   |   |       card-img-left-button.component.ts
            |   |       |   |   |
            |   |       |   |   +---card-img-right-button
            |   |       |   |   |       card-img-right-button.component.html
            |   |       |   |   |       card-img-right-button.component.scss
            |   |       |   |   |       card-img-right-button.component.spec.ts
            |   |       |   |   |       card-img-right-button.component.ts
            |   |       |   |   |
            |   |       |   |   +---card-img-top-button
            |   |       |   |   |       card-img-top-button.component.html
            |   |       |   |   |       card-img-top-button.component.scss
            |   |       |   |   |       card-img-top-button.component.spec.ts
            |   |       |   |   |       card-img-top-button.component.ts
            |   |       |   |   |
            |   |       |   |   +---card-simple-button
            |   |       |   |   |       card-simple-button.component.html
            |   |       |   |   |       card-simple-button.component.scss
            |   |       |   |   |       card-simple-button.component.spec.ts
            |   |       |   |   |       card-simple-button.component.ts
            |   |       |   |   |
            |   |       |   |   \---card-video-button
            |   |       |   |           card-video-button.component.html
            |   |       |   |           card-video-button.component.scss
            |   |       |   |           card-video-button.component.spec.ts
            |   |       |   |           card-video-button.component.ts
            |   |       |   |
            |   |       |   \---link
            |   |       |       +---card-img-left-link
            |   |       |       |       card-img-left-link.component.html
            |   |       |       |       card-img-left-link.component.scss
            |   |       |       |       card-img-left-link.component.spec.ts
            |   |       |       |       card-img-left-link.component.ts
            |   |       |       |
            |   |       |       +---card-img-right-link
            |   |       |       |       card-img-right-link.component.html
            |   |       |       |       card-img-right-link.component.scss
            |   |       |       |       card-img-right-link.component.spec.ts
            |   |       |       |       card-img-right-link.component.ts
            |   |       |       |
            |   |       |       +---card-img-top
            |   |       |       |       card-img-top.component.html
            |   |       |       |       card-img-top.component.scss
            |   |       |       |       card-img-top.component.spec.ts
            |   |       |       |       card-img-top.component.ts
            |   |       |       |
            |   |       |       +---card-simple-link
            |   |       |       |       card-simple-link.component.html
            |   |       |       |       card-simple-link.component.scss
            |   |       |       |       card-simple-link.component.spec.ts
            |   |       |       |       card-simple-link.component.ts
            |   |       |       |
            |   |       |       \---card-video-link
            |   |       |               card-video-link.component.html
            |   |       |               card-video-link.component.scss
            |   |       |               card-video-link.component.spec.ts
            |   |       |               card-video-link.component.ts
            |   |       |
            |   |       +---carrousel
            |   |       |       carrousel.component.html
            |   |       |       carrousel.component.scss
            |   |       |       carrousel.component.spec.ts
            |   |       |       carrousel.component.ts
            |   |       |
            |   |       +---image
            |   |       |       image.component.html
            |   |       |       image.component.scss
            |   |       |       image.component.spec.ts
            |   |       |       image.component.ts
            |   |       |
            |   |       \---video
            |   |               video.component.html
            |   |               video.component.scss
            |   |               video.component.spec.ts
            |   |               video.component.ts
            |   |
            |   \---services
            |           chat.service.ts
            |           formulario.service.ts
            |
            +---assets
            |       .gitkeep
            |
            +---environments
            |       environment.prod.ts
            |       environment.ts
            |
            +---models
            |       chat.model.ts
            |       client.model.ts
            |
            \---providers
                    Api.ts

